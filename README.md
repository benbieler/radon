```
 $ npm install -g radon-cli
```

![Logo of Radon](./docs/resources/radon.png "Radon")

 > A watcher and simplifier for npm.
 
## What is it? 
- Watches the ```package.json``` for changes (similar to the maven auto-import feature for intellij IDEA)
    - Installs new modules automatically and removes them from the node_modules/
- Suggests modules which you can install via prompt.
- Automatically saves to ```package.json```.
- Simple commands (clean/delete/clear, import/install)

## How to get it?

```
 $ npm install -g radon-cli
```

## Example

Enter ```radon``` in the CLI:

```$ radon```

Then a new prompt is going to open.

To install the package run (or import):

```$ command: install```

```$ command:  ++  Installed node_modules ++```

To install new modules, enter a comma separated string:

```$ command: react, redux```

```$ command: ++ react ++ ```

```$ command: ++ redux ++ ```

To uninstall modules, run the following: 

```$ command: redux -d ```

```$ command: -- redux -- ```

Change something in the package.json when Radon is turned on:

```$ command:  The package changed.```

To delete the entire ```node_modules``` folder run (or clean or clear):

```$ command: delete```

This means that the package.json was re-installed or, if you deleted something, that specific module will
be removed from the node_modules/ dir - all automatically. Magic.


## Contribution:

If you want to contribute to this project please first discuss your issue or feature request on [Gitter](www.gitter.com) or simply as an github issue.
The best thing to do is to create an alias: 
```
 alias x="npm run build && sudo npm install -g && radon"
```
Then simply run ```x``` to build and start the cli tool.

###### Always run this in the root dir (This is  a feature of Radon v2). 
