## Contribution guidelines

- Branch naming convention: { IssueID }-short-description.
- Commit naming convention: { IssueID } { Issue Type (e.g. Enhancement, Bug, etc.) } short description
- Run npm test