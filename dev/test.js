/**
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const http = require("http");

http.createServer(function (err, res) {
  res.writeHead(200);
  res.end("Ok")
}).listen(8099);