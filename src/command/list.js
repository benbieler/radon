/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const exec = require('child_process').exec;
const ora  = require('ora');

const log  = console.log;

const { list_command, list_event, loader_type }
           = require('../constants/const');

/**
 * List the installed modules.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param done
 */
const list = (done) => {
  let spinner = new ora({text: list_event, spinner: loader_type}).start();
  exec(list_command.join(' '), function (error, stdout) {
    log(stdout);
    spinner.stop();
    done();
  });
};

module.exports = list;