/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const spawn = require('child_process').spawn;
const ora   = require('ora');

const { save_install_event, save_installed_event, loader_type }
            = require('../constants/const');

/**
 * Exclude a command:
 * e.g. nowyou could install a module with the name of import and
 * that will not conflict with the actual import command.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param result
 * @param done
 */
const exclude = (result, done) => {
  const resArrExcl = result.command.replace('-exclude', '').split(',');
  resArrExcl.map((item) => {
    let spinner = new ora({text: save_install_event + item, spinner: loader_type}).start();
    const installModule = spawn('npm', ['install', '--save', item]);
    installModule.on('close', function () {
      spinner.succeed(save_installed_event + item).stop();
      installModule.kill();
    });
  });
  done()
};

module.exports = exclude;