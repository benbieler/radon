/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const spawn = require('child_process').spawn;
const ora   = require('ora');

const { install_event, install_modules_command, installed_event, loader_type }
            = require('../constants/const');

/**
 * Install the package.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 * @param done
 */
const install_all = (done) => {
  let spinner = new ora({text: install_event, spinner: loader_type}).start();
  const installModules = spawn('npm', install_modules_command);
  installModules.on('close', function () {
    spinner.succeed(installed_event).stop();
    installModules.kill();
    done()
  });
};

module.exports = install_all;