/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const spawn = require('child_process').spawn;
const ora   = require('ora');

const { save_install_event, save_installed_event, loader_type }
            = require('../constants/const');

/**
 * Install a specific node module.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param result
 * @param done
 */
const install_specific = (result, done) => {
  const resArr = result.command.split(',');
  resArr.map((item, i) => {
    let spinner = new ora({text: save_install_event + item, spinner: loader_type}).start();
    const installModule = spawn('npm', ['install', '--save', item]);
    installModule.on('close', function () {
      spinner.succeed(save_installed_event + item).stop();
      installModule.kill();
    });
  });
  done()
};

module.exports = install_specific;