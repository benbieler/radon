/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const spawn = require('child_process').spawn;
const ora   = require('ora');

const { delete_specific_event, deleted_specific_event, loader_type }
            = require('../constants/const');

/**
 * Delete a specific module
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param result
 * @param done
 */
const delete_specific = (result, done) => {
  const resArrDel = result.command.replace('-d', '').split(',');
  resArrDel.forEach((item) => {
    let spinner = new ora({text: delete_specific_event + item, spinner: loader_type}).start();
    const uninstallModules = spawn('npm', ['uninstall', '--save', item]);
    uninstallModules.on('close', function () {
      spinner.fail(deleted_specific_event + item).stop();
      uninstallModules.kill();
    });
  });
  done()
};

module.exports = delete_specific;