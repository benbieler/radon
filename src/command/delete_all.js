/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const spawn     = require('child_process').spawn;
const ora       = require('ora');

const { delete_event, deleted_event, loader_type }
                = require('../constants/const');

/**
 * Delete the node_modules/.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 * @param done
 */
const delete_all = (done) => {
  let spinner = new ora({text: delete_event, spinner: loader_type}).start();
  const deleteModules = spawn('sudo', ['rm', '-rf', 'node_modules/']);
  deleteModules.on('close', function () {
    spinner.fail(deleted_event).stop();
    deleteModules.kill();
    done();
  });
};

module.exports = delete_all;