/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const chalk         = require('chalk');
const exec          = require('child_process').exec;
const { change_event, uninstall_module_command, install_modules_command }
                    = require('../../constants/const');
const ora           = require('ora');

const log           = console.log;

/**
 * deps.
 *
 * @author Ben Bieler <ben@benbieler.com>
 */
const deps = (diff1) => {
	if (diff1 === undefined) {
		exec(install_modules_command, () => {
			new ora({text: change_event, spinner: 'arc'}).start().info().stop();
		});
	} else if (Object.values(diff1)[0][1] === 0) {

		let newOrder = '';

		Object.keys(diff1).map((x) => newOrder + ' ' + x.toString());

		exec(uninstall_module_command + newOrder, () => {
			new ora({text: 'Deleting module: ' + Object.keys(diff1)[0], spinner: 'arc'}).start().fail().stop();
		});
	}
};

module.exports = deps;