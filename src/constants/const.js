/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

//DEFAULT:
const target 									 = 'package.json';
const welcome_message 				 = '\n Radon v1.1.0 \n (c) 2017 Ben Bieler <benbieler.com> ' +
                        			   '\n\n www.github.com/benbieler/radon \n';

// ERROR types:
const syntax_error						 = 'SyntaxError';
const type_error	 						 = 'TypeError';

//LOADER type:
const loader_type							 = 'arc';

//ERROR messages:
const ext_error                = 'This isn\'t a node project. Please make sure that you are in the root directory or run: npm init';
const version_error            = 'This cli tool was tested with node v4 or higher. It\'s advised to update your node version.';
const json_validation_error 	 = 'Awaiting valid json.';

//EVENTS:
const change_event    				 = 'The package changed. ( ' + new Date().getHours() + ':' + new Date().getMinutes() + 'h )';
const init_event							 = 'Running the init radon script';
const install_event   				 = 'Installing the node_modules/';
const installed_event 				 = 'Installed the node_modules/';

const save_install_event   		 = 'Installing the module: ';
const save_installed_event 		 = 'Installed the module: ';

const delete_event   					 = 'Deleting the node_modules/';
const deleted_event  					 = 'Deleted the node_modules/';

const delete_specific_event    = 'Deleting the module: ';
const deleted_specific_event   = 'Deleted the module: ';

const list_event   				     = 'Listing in progress';

//COMMANDS:
const install_modules_command  = ['install'];
const list_command 						 = ['npm', 'list'];

module.exports = {
	target,
	ext_error,
	version_error,
	change_event,
	install_event,
	delete_event,
	install_modules_command,
	syntax_error,
	json_validation_error,
	init_event,
	list_command,
	list_event,
  save_install_event,
  save_installed_event,
  installed_event,
  deleted_event,
  deleted_specific_event,
	type_error,
	loader_type,
  delete_specific_event,
	welcome_message
};


// TODO: The specific delete feature is kind of buggy