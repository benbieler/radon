#!/usr/bin/env node --harmony

/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */


'use strict';

const chokidar      = require('chokidar');
const chalk         = require('chalk');
const jsondiffpatch = require('jsondiffpatch');
const fs            = require('fs');
const exec          = require('child_process').exec;
const ora           = require('ora');

const run_prompt    = require('./prompt');
const { target, ext_error, version_error, welcome_message, loader_type , init_event }
									  = require('./constants/const');
const deps          = require('./util/diffing/deps');
const { getData }   = require('./config/dry');

const log           = console.log;

/**
 * Entry point.
 *
 * @author Ben Bieler <ben@benbieler.com>
 *
 */
const main = () => {

  // Since node only gives you 10 emitters you need to manually set them to infinity
  require('events').EventEmitter.defaultMaxListeners = Infinity;


  log(chalk.inverse(welcome_message));

	const watcherConfig        = chokidar.watch(target);

	let packageDependencies    = getData();

	watcherConfig.on('change', () => {

    //Get the current status of the package => needed to diff later
		let changedPackageDependencies = getData();

		deps(jsondiffpatch.diff(packageDependencies, changedPackageDependencies));
	});

  run_prompt();
};

const existence_checker = () => {
	return fs.existsSync(target);
};

// Check node version and if it even is in a node project.
if (process.version.match(/^v(\d+\.\d+)/)[1] <= 4) {
	log(chalk.yellow(version_error));
	main();
} else if (!existence_checker()) {
	log(chalk.red(ext_error));
} else {

  const command = JSON.parse(fs.readFileSync(target).toString()).scripts.radon;

  if ( typeof command != undefined) {
    let spinner = new ora({text: init_event, spinner: loader_type}).start();
    exec(command, function (error) {
      if (error)
        throw new Error;
      spinner.stop();
      main()
    });
  } else {
    main();
  }
}

module.exports = main;