/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const prompt           = require('prompt');
const chalk            = require('chalk');
const ora              = require('ora');

const delete_all       = require('./command/delete_all');
const install_all      = require('./command/install_all');
const delete_specific  = require('./command/delete_specific');
const list             = require('./command/list');
const exclude          = require('./command/exclude');
const install_specific = require('./command/install_specific');
const { type_error }   = require('./constants/const');

const log              = console.log;

/**
 * runPrompt.
 *
 * @author Ben Bieler <ben@benbieler.com>
 */
const run_prompt = () => {

	prompt.message = '';
	prompt.delimiter = '';

	prompt.start();

	prompt.get({
		properties: {
			command: {
				command: chalk.grey('radon : ')
			}
		}
	}, (err, result) => {
		try {

      if (result.command == 'clean' || result.command == 'delete' || result.command == 'clear') {
				delete_all(function() {
          run_prompt()
        });
			}

			else if (result.command == 'install' || result.command == 'import') {
        install_all(function () {
          run_prompt();
        });
			}

			else if (result.command.includes(' -d')) {
        delete_specific(result, function() {
          run_prompt()
        })
			}

      else if (result.command == 'list') {
        list(function() {
          run_prompt()
        })
			}

			else if (result.command.includes(' -exclude')) {
        exclude(result, function() {
          run_prompt()
        })
			}

			else {
			  install_specific(result, function() {
          run_prompt()
        })
			}

		} catch (e) {
			if (e.name == type_error) {
				log('Bye.');
			}
		}
	});
};

module.exports = run_prompt;