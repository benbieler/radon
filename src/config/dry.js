/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const fs             = require('fs');
const chalk          = require('chalk');
const ora            = require('ora');
const { target, json_validation_error, syntax_error }
                     = require('../constants/const');
const runPrompt      = require('../prompt');

const getData = () => {
	try {
		return Object.assign({},
            JSON.parse(fs.readFileSync(target).toString()).dependencies,
            JSON.parse(fs.readFileSync(target).toString()).devDependencies);
	}
	catch (e) {
		if (e.name == syntax_error) {
			new ora({text: json_validation_error, spinner: 'arc'}).start().warn().stop();
			runPrompt();
		}
	}
};

module.exports = {
	getData
};