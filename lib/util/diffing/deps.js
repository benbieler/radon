/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var chalk = require('chalk');
var exec = require('child_process').exec;

var _require = require('../../constants/const'),
    change_event = _require.change_event,
    uninstall_module_command = _require.uninstall_module_command,
    install_modules_command = _require.install_modules_command;

var ora = require('ora');

var log = console.log;

/**
 * deps.
 *
 * @author Ben Bieler <ben@benbieler.com>
 */
var deps = function deps(diff1) {
	if (diff1 === undefined) {
		exec(install_modules_command, function () {
			new ora({ text: change_event, spinner: 'arc' }).start().info().stop();
		});
	} else if (Object.values(diff1)[0][1] === 0) {

		var newOrder = '';

		Object.keys(diff1).map(function (x) {
			return newOrder + ' ' + x.toString();
		});

		exec(uninstall_module_command + newOrder, function () {
			new ora({ text: 'Deleting module: ' + Object.keys(diff1)[0], spinner: 'arc' }).start().fail().stop();
		});
	}
};

module.exports = deps;