/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var prompt = require('prompt');
var chalk = require('chalk');
var ora = require('ora');

var delete_all = require('./command/delete_all');
var install_all = require('./command/install_all');
var delete_specific = require('./command/delete_specific');
var list = require('./command/list');
var exclude = require('./command/exclude');
var install_specific = require('./command/install_specific');

var _require = require('./constants/const'),
    type_error = _require.type_error;

var log = console.log;

/**
 * runPrompt.
 *
 * @author Ben Bieler <ben@benbieler.com>
 */
var run_prompt = function run_prompt() {

  prompt.message = '';
  prompt.delimiter = '';

  prompt.start();

  prompt.get({
    properties: {
      command: {
        command: chalk.grey('radon : ')
      }
    }
  }, function (err, result) {
    try {

      // GOOD
      if (result.command == 'clean' || result.command == 'delete' || result.command == 'clear') {
        delete_all(function () {
          run_prompt();
        });
      }

      // GOOD
      else if (result.command == 'install' || result.command == 'import') {
          install_all(function () {
            run_prompt();
          });
        }

        // GOOD
        else if (result.command == '') {
            run_prompt();
          } else if (result.command.includes(' -d')) {
            delete_specific(result, function () {
              run_prompt();
            });
          }

          // GOOD
          else if (result.command == 'list') {
              list(function () {
                run_prompt();
              });
            } else if (result.command.includes(' -exclude')) {
              exclude(result, function () {
                run_prompt();
              });
            } else {
              install_specific(result, function () {
                run_prompt();
              });
            }
    } catch (e) {
      if (e.name == type_error) {
        log('Bye.');
      }
    }
  });
};

module.exports = run_prompt;