/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var spawn = require('child_process').spawn;
var ora = require('ora');

var _require = require('../constants/const'),
    delete_event = _require.delete_event,
    deleted_event = _require.deleted_event,
    loader_type = _require.loader_type;

/**
 * Delete the node_modules/.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 * @param done
 */


var delete_all = function delete_all(done) {
  var spinner = new ora({ text: delete_event, spinner: loader_type }).start();
  var deleteModules = spawn('sudo', ['rm', '-rf', 'node_modules/']);
  deleteModules.on('close', function () {
    spinner.fail(deleted_event).stop();
    deleteModules.kill();
    done();
  });
};

module.exports = delete_all;