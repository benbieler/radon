/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var exec = require('child_process').exec;
var ora = require('ora');

var log = console.log;

var _require = require('../constants/const'),
    list_command = _require.list_command,
    list_event = _require.list_event,
    loader_type = _require.loader_type;

/**
 * List the installed modules.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param done
 */


var list = function list(done) {
  var spinner = new ora({ text: list_event, spinner: loader_type }).start();
  exec(list_command.join(' '), function (error, stdout) {
    log(stdout);
    spinner.stop();
    done();
  });
};

module.exports = list;