/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var spawn = require('child_process').spawn;
var ora = require('ora');

var _require = require('../constants/const'),
    save_install_event = _require.save_install_event,
    save_installed_event = _require.save_installed_event,
    loader_type = _require.loader_type;

/**
 * Exclude a command:
 * e.g. nowyou could install a module with the name of import and
 * that will not conflict with the actual import command.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param result
 * @param done
 */


var exclude = function exclude(result, done) {
  var resArrExcl = result.command.replace('-exclude', '').split(',');
  resArrExcl.map(function (item) {
    var spinner = new ora({ text: save_install_event + item, spinner: loader_type }).start();
    var installModule = spawn('npm', ['install', '--save', item]);
    installModule.on('close', function () {
      spinner.succeed(save_installed_event + item).stop();
      installModule.kill();
    });
  });
  done();
};

module.exports = exclude;