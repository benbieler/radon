/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var spawn = require('child_process').spawn;
var ora = require('ora');

var _require = require('../constants/const'),
    delete_specific_event = _require.delete_specific_event,
    deleted_specific_event = _require.deleted_specific_event,
    loader_type = _require.loader_type;

/**
 * Delete a specific module
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param result
 * @param done
 */


var delete_specific = function delete_specific(result, done) {
  var resArrDel = result.command.replace('-d', '').split(',');
  resArrDel.forEach(function (item) {
    var spinner = new ora({ text: delete_specific_event + item, spinner: loader_type }).start();
    var uninstallModules = spawn('npm', ['uninstall', '--save', item]);
    uninstallModules.on('close', function () {
      spinner.fail(deleted_specific_event + item).stop();
      uninstallModules.kill();
    });
  });
  done();
};

module.exports = delete_specific;