/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var spawn = require('child_process').spawn;
var ora = require('ora');

var _require = require('../constants/const'),
    install_event = _require.install_event,
    install_modules_command = _require.install_modules_command,
    installed_event = _require.installed_event,
    loader_type = _require.loader_type;

/**
 * Install the package.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 * @param done
 */


var install_all = function install_all(done) {
  var spinner = new ora({ text: install_event, spinner: loader_type }).start();
  var installModules = spawn('npm', install_modules_command);
  installModules.on('close', function () {
    spinner.succeed(installed_event).stop();
    installModules.kill();
    done();
  });
};

module.exports = install_all;