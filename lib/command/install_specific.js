/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var spawn = require('child_process').spawn;
var ora = require('ora');

var _require = require('../constants/const'),
    save_install_event = _require.save_install_event,
    save_installed_event = _require.save_installed_event,
    loader_type = _require.loader_type;

/**
 * Install a specific node module.
 *
 * @author  Ben Bieler <ben@benbieler.com>
 *
 * @param result
 * @param done
 */


var install_specific = function install_specific(result, done) {
  var resArr = result.command.split(',');
  resArr.map(function (item, i) {
    var spinner = new ora({ text: save_install_event + item, spinner: loader_type }).start();
    var installModule = spawn('npm', ['install', '--save', item]);
    installModule.on('close', function () {
      spinner.succeed(save_installed_event + item).stop();
      installModule.kill();
    });
  });
  done();
};

module.exports = install_specific;