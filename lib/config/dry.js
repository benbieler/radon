/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var fs = require('fs');
var chalk = require('chalk');
var ora = require('ora');

var _require = require('../constants/const'),
    target = _require.target,
    json_validation_error = _require.json_validation_error,
    syntax_error = _require.syntax_error;

var runPrompt = require('../prompt');

var getData = function getData() {
	try {
		return Object.assign({}, JSON.parse(fs.readFileSync(target).toString()).dependencies, JSON.parse(fs.readFileSync(target).toString()).devDependencies);
	} catch (e) {
		if (e.name == syntax_error) {
			new ora({ text: json_validation_error, spinner: 'arc' }).start().warn().stop();
			runPrompt();
		}
	}
};

module.exports = {
	getData: getData
};