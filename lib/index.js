#!/usr/bin/env node --harmony


/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var chokidar = require('chokidar');
var chalk = require('chalk');
var jsondiffpatch = require('jsondiffpatch');
var fs = require('fs');
var exec = require('child_process').exec;
var ora = require('ora');

var run_prompt = require('./prompt');

var _require = require('./constants/const'),
    target = _require.target,
    ext_error = _require.ext_error,
    version_error = _require.version_error,
    welcome_message = _require.welcome_message,
    loader_type = _require.loader_type,
    init_event = _require.init_event;

var deps = require('./util/diffing/deps');

var _require2 = require('./config/dry'),
    getData = _require2.getData;

var log = console.log;

/**
 * Entry point.
 *
 * @author Ben Bieler <ben@benbieler.com>
 *
 */
var main = function main() {

  require('events').EventEmitter.defaultMaxListeners = Infinity;

  log(chalk.inverse(welcome_message));

  var watcherConfig = chokidar.watch(target);

  var packageDependencies = getData();

  watcherConfig.on('change', function () {

    //Get the current status of the package => needed to diff later
    var changedPackageDependencies = getData();

    deps(jsondiffpatch.diff(packageDependencies, changedPackageDependencies));
  });

  run_prompt();
};

var existence_checker = function existence_checker() {
  return fs.existsSync(target);
};

// Check node version and if it even is in a node project.
if (process.version.match(/^v(\d+\.\d+)/)[1] <= 4) {
  log(chalk.yellow(version_error));
  main();
} else if (!existence_checker()) {
  log(chalk.red(ext_error));
} else {

  var command = JSON.parse(fs.readFileSync(target).toString()).scripts.radon;

  if ((typeof command === 'undefined' ? 'undefined' : _typeof(command)) != undefined) {
    var spinner = new ora({ text: init_event, spinner: loader_type }).start();
    exec(command, function (error) {
      if (error) throw new Error();
      spinner.stop();
      main();
    });
  } else {
    main();
  }
}

module.exports = main;