/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

//DEFAULT:

var target = 'package.json';
var welcome_message = '\n Radon v1.1.0 \n (c) 2017 Ben Bieler <benbieler.com> ' + '\n\n www.github.com/benbieler/radon \n';

// ERROR types:
var syntax_error = 'SyntaxError';
var type_error = 'TypeError';

//LOADER type:
var loader_type = 'arc';

//ERROR messages:
var ext_error = 'This isn\'t a node project. Please make sure that you are in the root directory or run: npm init';
var version_error = 'This cli tool was tested with node v4 or higher. It\'s advised to update your node version.';
var json_validation_error = 'Awaiting valid json.';

//EVENTS:
var change_event = 'The package changed. ( ' + new Date().getHours() + ':' + new Date().getMinutes() + 'h )';
var init_event = 'Running the init radon script';
var install_event = 'Installing the node_modules/';
var installed_event = 'Installed the node_modules/';

var save_install_event = 'Installing the module: ';
var save_installed_event = 'Installed the module: ';

var delete_event = 'Deleting the node_modules/';
var deleted_event = 'Deleted the node_modules/';

var delete_specific_event = 'Deleting the module: ';
var deleted_specific_event = 'Deleted the module: ';

var list_event = 'Listing in progress';

//COMMANDS:
var install_modules_command = ['install'];
var list_command = ['npm', 'list'];

module.exports = {
	target: target,
	ext_error: ext_error,
	version_error: version_error,
	change_event: change_event,
	install_event: install_event,
	delete_event: delete_event,
	install_modules_command: install_modules_command,
	syntax_error: syntax_error,
	json_validation_error: json_validation_error,
	init_event: init_event,
	list_command: list_command,
	list_event: list_event,
	save_install_event: save_install_event,
	save_installed_event: save_installed_event,
	installed_event: installed_event,
	deleted_event: deleted_event,
	deleted_specific_event: deleted_specific_event,
	type_error: type_error,
	loader_type: loader_type,
	delete_specific_event: delete_specific_event,
	welcome_message: welcome_message
};

// TODO: The specific delete feature is kind of buggy