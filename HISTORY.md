# History of Radon

## v1.0.0 / 2017-04-16
- Watches the ```package.json``` for changes (similar to the maven auto-import feature for intellij IDEA)
    - Installs new modules automatically and removes them from the node_modules/
- Suggests modules which you can install via prompt.
- Automatically saves to ```package.json``` (disable via -off flag)
- Simple commands (clean/delete/clear, import/install)

## v1.1.0 / 2017-04-30 
- Change the status view