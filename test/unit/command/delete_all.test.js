/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const expect     = require('chai').expect;
const delete_all = require('../../../src/command/delete_all');
const prompt     = require('../../../src/prompt');

describe('delete_all()', () => {
  it('should return the prompt', () => {
    expect(delete_all(prompt())).to.equal(prompt());
  });
});