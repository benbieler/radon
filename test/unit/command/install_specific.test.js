/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const expect    = require('chai').expect;

const install_specific = require('../../../src/command/install_specific');
const prompt          = require('../../../src/prompt');

describe('install_specific()', () => {
  it('should return the prompt', () => {
    const command_body = { command: 'react' };
    expect(install_specific(command_body, prompt())).to.be.equal(prompt())
  })
});