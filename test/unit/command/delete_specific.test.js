/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const expect    = require('chai').expect;

const delete_specific = require('../../../src/command/delete_specific');
const prompt          = require('../../../src/prompt');

describe('delete_specific()', () => {
  it('should return the prompt', () => {
    const command_body = { command: 'react' };
    expect(delete_specific(command_body, prompt())).to.equal(prompt());
  });
});
