/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const expect    = require('chai').expect;

const exclude = require('../../../src/command/exclude');
const prompt     = require('../../../src/prompt');

describe('exclude()', () => {
  it('should return the prompt', () => {
    const command_body = { command: 'import' };
    expect(exclude(command_body, prompt())).to.be.equal(prompt())
  })
});