/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const expect    = require('chai').expect;

const install_all = require('../../../src/command/install_all');
const prompt      = require('../../../src/prompt');

describe('install_all()', () => {
  it('should return the prompt', () => {
    expect(install_all(prompt())).to.be.equal(prompt())
  })
});
