/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';


const main      = require('../../src/index');
const runPrompt = require('../../src/prompt');
const expect    = require('chai').expect;

describe('main()', () => {
  it('should run the prompt', () => {
    expect(main()).to.equal(runPrompt())
  })
});