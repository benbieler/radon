/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 */

'use strict';

const runPrompt = require('../../src/prompt');
const should    = require('chai').should();

describe('runPrompt()', () => {
  it('should not return anything', () => {
    should.equal(runPrompt(), undefined);
  })
});
